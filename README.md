# SVG icons for QGIS

A collection of SVG icons designed in [Inkscape](https://inkscape.org) for [QGIS](https://qgis.org). Feel free to use them how you like.

No attribution is necessary, donations are highly appreciated. Paypal and Stripe links are here: https://spatialised.net/about
